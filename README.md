1. Установить СУБД Postgresql и создать БД outgohome
2. Поменять настройки БД в application.properties
3. Попробовать запустить проект mvn spring-boot:run
4. Если есть ошибки связанные с таблицами public (flyway), то выполнить mvn flyway:clean, после чего опять перезапустить проект

<h1>Start project</h1>

mvn spring-boot:run
<h1>Docker</h1>
<h3>Deploy</h3>

docker-compose up

<h4>shutdown</h4>

docker-compose down

<h4>start</h4>

docker-compose start

<h3>FLYWAY</h3>

Перед стартом приложения необходимо убедиться в наличии БД

Настройки к БД прописаны в application.properties

mvn flyway:clean - очистить все схемы БД

После запуска flyway смотрит в db.migration и выполняет не пройденные ранее скрипты