package app.security.jwt;

import app.model.Role;
import app.model.Status;
import app.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class JwtUserFactory {
    private JwtUserFactory() {

    }

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                user.getEmail(),
//                user.getStatus().equals(Status.ACTIVE),
                user.getUpdated(),
                mapToGrantedAuthorities(new ArrayList<>(user.getRoles()))
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> userRoles) {
        return userRoles.stream()
                .map(role ->
                        new SimpleGrantedAuthority(role.getName())
                ).collect(Collectors.toList());
    }



    public static User convertJwtUserToUser(JwtUser jwtUser) {
        User user = new User();
        user.setId(jwtUser.getId());
        user.setFirstName(jwtUser.getFirstName());
        user.setLastName(jwtUser.getLastName());
        user.setEmail(jwtUser.getEmail());
        user.setPassword(jwtUser.getPassword());

        return user;
    }
}
