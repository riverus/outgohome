package app.dao;

import app.model.Category;
import app.model.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface RecordRepository extends JpaRepository<Record, Long> {
    List<Record> findAllByDateBetween(LocalDate dateFrom, LocalDate dateTo);

    List<Record> findAllByDateAfterAndUserId(LocalDate dateFrom, Long userId);

    List<Record> findAllByDateBeforeAndUserId(LocalDate dateTo, Long userId);

    List<Record> findAllByDateBetweenAndUserId(LocalDate dateFrom, LocalDate dateTo, Long userId);

    void deleteAllByCategory(Category category);

    List<Record> findAllByUserId(Long userId);

    @Query("SELECT DISTINCT date from Record WHERE user_id=:userId")
    List<LocalDate> selectAllDatesByUserId(@Param("userId") Long userId);
}
