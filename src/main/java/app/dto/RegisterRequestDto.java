package app.dto;

import app.model.Avatar;
import app.model.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterRequestDto {

    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private MultipartFile image;

    public User toUser(){
        User user = new User();
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password);
        Avatar avatar=new Avatar();
        try {
            avatar.setImage(image.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        user.setAvatar(avatar);

        return user;
    }

}
