package app.dto;

import lombok.Data;

import java.util.Objects;

@Data
public class StatisticItem {

        private String category;
        private Float price;

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                StatisticItem that = (StatisticItem) o;
                return Objects.equals(category, that.category);
        }

        @Override
        public int hashCode() {
                return Objects.hash(category);
        }

        public void addPrice(Float price) {
                this.price = this.price + price;
        }
}
