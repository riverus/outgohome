package app.dto;

import lombok.Data;

import java.util.*;

@Data
public class StatisticDto {
    private String date;
    private List<StatisticItem> statistic;

    public StatisticDto() {
        this.statistic = new ArrayList<>();

    }


}


