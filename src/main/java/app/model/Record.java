package app.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Record {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate date;
    private LocalDateTime created;
    @OneToOne(cascade = CascadeType.MERGE)
    private Category category;
    private Float value;
    private String comments;
    private Long userId;
    @Enumerated(EnumType.STRING)
    private ItemType itemType;

}
