package app.controller.v1;

import app.dao.CategoryRepository;
import app.dao.RecordRepository;
import app.model.Category;
import app.model.User;
import app.security.jwt.JwtUser;
import app.security.jwt.JwtUserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
@CrossOrigin
@RestController
@RequestMapping("/api/v1/categories")
public class CategoryControllerV1 {
    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    RecordRepository recordRepository;

    @GetMapping
    public List<Category> getAllCategories(@AuthenticationPrincipal JwtUser jwtUser) {
        User user = JwtUserFactory.convertJwtUserToUser(jwtUser);
        List<Category> allByUserIdOrIsDefaultTrue = categoryRepository.getAllByUserIdOrIsDefaultTrue(user.getId());

        return allByUserIdOrIsDefaultTrue;
    }

    @GetMapping("{id}")
    private Category retrieveCategory(@PathVariable long id) throws CategoryNotFoundException {
        Optional<Category> category = categoryRepository.findById(id);

        if (!category.isPresent())
            throw new CategoryNotFoundException("id-" + id);

        return category.get();
    }

    @DeleteMapping("{id}")
    @Transactional
    public void deleteCategory(@PathVariable long id) throws CategoryNotFoundException {
        Category category = this.retrieveCategory(id);
        if (category.getIsDefault()==null) {
            recordRepository.deleteAllByCategory(category);
            categoryRepository.deleteById(id);
        }
    }

    @PostMapping
    public ResponseEntity<Object> createCategory(@RequestBody Category category, @AuthenticationPrincipal JwtUser jwtUser) {
        User user = JwtUserFactory.convertJwtUserToUser(jwtUser);
        category.setUserId(user.getId());
        Category savedCategory = categoryRepository.save(category);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedCategory.getId()).toUri();

        return ResponseEntity.created(location).build();

    }

    @PutMapping("{id}")
    public ResponseEntity<Object> updateRecord(@RequestBody Category category, @PathVariable long id, @AuthenticationPrincipal JwtUser jwtUser) {
        User user = JwtUserFactory.convertJwtUserToUser(jwtUser);
        Optional<Category> categoryOptional = categoryRepository.findById(id);

        if (!categoryOptional.isPresent())
            return ResponseEntity.notFound().build();

        category.setId(id);
        category.setUserId(user.getId());
        categoryRepository.save(category);
        return ResponseEntity.noContent().build();
    }
}
