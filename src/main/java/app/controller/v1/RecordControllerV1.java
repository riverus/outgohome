package app.controller.v1;

import app.dao.CategoryRepository;
import app.dao.RecordRepository;
import app.model.Category;
import app.model.Record;
import app.model.User;
import app.security.jwt.JwtUser;
import app.security.jwt.JwtUserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/records")
public class RecordControllerV1 {


    RecordRepository recordRepository;
    CategoryRepository categoryRepository;

    public RecordControllerV1() {
    }

    @Autowired
    public RecordControllerV1(RecordRepository recordRepository, CategoryRepository categoryRepository) {
        this.recordRepository = recordRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping
    public List<Record> getAllRecords(@RequestParam(value = "dateFrom", required = false) String dateFrom,
                                      @RequestParam(value = "dateTo", required = false) String dateTo,
                                      @AuthenticationPrincipal JwtUser jwtUser) {

        User user = JwtUserFactory.convertJwtUserToUser(jwtUser);

        System.out.println("date from: " + dateFrom);
        System.out.println("date to: " + dateTo);
        if (dateFrom != null && !dateFrom.isEmpty()
                && dateTo != null && !dateTo.isEmpty()) {
            return recordRepository.findAllByDateBetweenAndUserId(LocalDate.parse(dateFrom), LocalDate.parse(dateTo), user.getId());
        }
        if (dateFrom != null && !dateFrom.isEmpty()) {
            return recordRepository.findAllByDateAfterAndUserId(LocalDate.parse(dateFrom), user.getId());
        }
        if (dateTo != null && !dateTo.isEmpty()) {
            return recordRepository.findAllByDateBeforeAndUserId(LocalDate.parse(dateTo), user.getId());
        }

        LocalDate today = LocalDate.now();
        LocalDate startOfMonth = today.withDayOfMonth(1);

        return recordRepository.findAllByDateBetweenAndUserId(startOfMonth, today, user.getId());
    }

    @GetMapping("{id}")
    public Record retrieveRecord(@PathVariable long id) throws RecordNotFoundException {
        Optional<Record> record = recordRepository.findById(id);

        if (!record.isPresent())
            throw new RecordNotFoundException("id-" + id);

        return record.get();
    }

    @DeleteMapping("{id}")
    public void deleteRecord(@PathVariable long id) {
        recordRepository.deleteById(id);
    }

    @PostMapping
    public ResponseEntity<Object> createRecord(@RequestBody Record record, @AuthenticationPrincipal JwtUser jwtUser) {
        User user = JwtUserFactory.convertJwtUserToUser(jwtUser);

        record.setCreated(LocalDateTime.now());
        record.setUserId(user.getId());

        Record savedRecord = recordRepository.save(record);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedRecord.getId()).toUri();

        return ResponseEntity.created(location).build();

    }

    @PutMapping("{id}")
    public ResponseEntity<Object> updateRecord(@RequestBody Record record, @PathVariable long id, @AuthenticationPrincipal JwtUser jwtUser) {
        User user = JwtUserFactory.convertJwtUserToUser(jwtUser);
        Optional<Record> recordOptional = recordRepository.findById(id);

        if (!recordOptional.isPresent())
            return ResponseEntity.notFound().build();
        Category category = null;
        if (record.getCategory().getId() != null) {
            category = categoryRepository.findById(record.getCategory().getId()).orElse(null);
        }
        if (category != null) {
            record.setCategory(category);
        } else {
            record.setCategory(recordOptional.get().getCategory());
        }
        record.setId(id);
        record.setUserId(user.getId());
        recordRepository.save(record);
        return ResponseEntity.noContent().build();
    }
}
