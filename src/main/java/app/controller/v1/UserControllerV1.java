package app.controller.v1;

import app.dto.UserDto;
import app.model.User;
import app.security.jwt.JwtTokenProvider;
import app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/user")
public class UserControllerV1 {

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    HttpServletRequest request;

    @Autowired
    UserService userService;

    @GetMapping
    public ResponseEntity getUser(){

        String jwtToken = jwtTokenProvider.resolveToken(request).orElseThrow(() -> new NullPointerException("Jwt token is null"));

        String username = jwtTokenProvider.getUsername(jwtToken);

        UserDto userDto = UserDto.fromUser(userService.findByUsername(username));
        userDto.setToken(jwtToken);
        Map<Object, Object> response = new HashMap<>();
        response.put("user", userDto);
        return ResponseEntity.ok(response);

    }
}
