package app.controller.v1;

import app.dao.RecordRepository;
import app.dto.StatisticDto;
import app.dto.StatisticItem;
import app.model.Record;
import app.model.User;
import app.security.jwt.JwtUser;
import app.security.jwt.JwtUserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/statistic")
public class StatisticControllerV1 {

    @Autowired
    RecordRepository recordRepository;


    @GetMapping
    public List<StatisticDto> getAllStatistic(@RequestParam(value = "dateFrom", required = false) String dateFrom,
                                              @RequestParam(value = "dateTo", required = false) String dateTo,
                                              @RequestParam(value = "category",required = false) String category,
                                              @AuthenticationPrincipal JwtUser jwtUser) {

        User user = JwtUserFactory.convertJwtUserToUser(jwtUser);
//TODO добавить обработку даты . Разобраться с правильной структурой данных
//        if (dateFrom != null && dateTo != null) {
//            return recordRepository.findAllByDateBetweenAndUserId(LocalDate.parse(dateFrom), LocalDate.parse(dateTo), user.getId());
//        }
//        if (dateFrom != null) {
//            return recordRepository.findAllByDateAfterAndUserId(LocalDate.parse(dateFrom), user.getId());
//        }
//        if (dateTo != null) {
//            return recordRepository.findAllByDateBeforeAndUserId(LocalDate.parse(dateTo), user.getId());
//        }


        if (category != null && !category.isEmpty()) {
            return getStatisticByCategory(user, category);
        }

        return getStatistic(user);
    }

    private List<StatisticDto> getStatistic(User user) {
        List<Record> allRecords = recordRepository.findAllByUserId(user.getId());
        List<LocalDate> localDates = recordRepository.selectAllDatesByUserId(user.getId());

        List<StatisticDto> statisticDtoList = new ArrayList<>();
        for (LocalDate localDate : localDates) {
            StatisticDto statisticDto = new StatisticDto();

            List<StatisticItem> statistic = statisticDto.getStatistic();


            for (Record record : allRecords) {
                if (record.getDate().equals(localDate)) {

                    StatisticItem statisticItem = new StatisticItem();
                    statisticItem.setCategory(record.getCategory().getName());

                    if (statistic.contains(statisticItem)) {
                        StatisticItem statisticItemInner = statistic.get(statistic.indexOf(statisticItem));
                        statisticItemInner.addPrice(record.getValue());
                    } else {
                        statisticItem.setPrice(record.getValue());
                        statistic.add(statisticItem);
                    }

                }
            }

            statisticDto.setDate(localDate.toString());
            statisticDtoList.add(statisticDto);
        }
        return statisticDtoList;
    }



    private List<StatisticDto> getStatisticByCategory(User user, String category) {

        List<Record> allRecords = recordRepository.findAllByUserId(user.getId());
        List<LocalDate> localDates = recordRepository.selectAllDatesByUserId(user.getId());

        List<StatisticDto> statisticDtoList = new ArrayList<>();
        for (LocalDate localDate : localDates) {
            StatisticDto statisticDto = new StatisticDto();

            List<StatisticItem> statistic = statisticDto.getStatistic();


            for (Record record : allRecords) {
                if (record.getDate().equals(localDate) && record.getCategory().getName().equals(category)) {

                    StatisticItem statisticItem = new StatisticItem();
                    statisticItem.setCategory(record.getCategory().getName());

                    if (statistic.contains(statisticItem)) {
                        StatisticItem statisticItemInner = statistic.get(statistic.indexOf(statisticItem));
                        statisticItemInner.addPrice(record.getValue());
                    } else {
                        statisticItem.setPrice(record.getValue());
                        statistic.add(statisticItem);
                    }
                }
            }
            if (!statisticDto.getStatistic().isEmpty()) {
                statisticDto.setDate(localDate.toString());
                statisticDtoList.add(statisticDto);
            }
        }
        return statisticDtoList;
    }
}
