package app.service.impl;

import app.dao.RoleRepository;
import app.dao.UserDetailsRepository;
import app.model.Role;
import app.model.Status;
import app.model.User;
import app.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserDetailsRepository userDetailsRepository;

    private final RoleRepository roleRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserDetailsRepository userDetailsRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDetailsRepository = userDetailsRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public User register(User user) {
        Role roleUser = roleRepository.findByName("USER");
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(roleUser);

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(userRoles);
        user.setStatus(Status.ACTIVE);

        User registeredUser = userDetailsRepository.save(user);
        log.info("IN register: user {} successfully registered", registeredUser);

        return registeredUser;
    }

    @Override
    public List<User> getAll() {
        List<User> userList = userDetailsRepository.findAll();
        log.info("IN getAll: {} users found", userList.size());
        return userList;
    }

    @Override
    @Transactional
    public User findByUsername(String username) {
        User user = userDetailsRepository.findByUsername(username);
        log.info("IN findByUsername: user is found by username {}", username);
        return user;
    }

    @Override
    public User findById(Long id) {
        User user = userDetailsRepository.findById(id).orElse(null);

        if (user == null) {
            log.warn("IN findById: no user found by id {}", id);
            return null;
        }
        log.info("IN findById: user {} is found by id {}", user, id);

        return user;
    }

    @Override
    public void delete(Long id) {
        userDetailsRepository.deleteById(id);
        log.info("IN delete: user with id {} successfully deleted", id);

    }
}
