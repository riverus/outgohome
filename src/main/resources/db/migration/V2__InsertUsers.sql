insert into roles(
            id, created, status, updated, name)
    values (1, '2019-09-15 18:53:41.167', 'ACTIVE', '2019-09-15 18:53:41.167', 'ADMINS');
insert into roles(
            id, created, status, updated, name)
    values (2, '2019-09-15 18:53:41.167', 'ACTIVE', '2019-09-15 18:53:41.167', 'USERS');

insert into users(
            id, created, status, updated, email, first_name, last_name, password,
            username)
    values (9999, '2019-09-15 18:53:41.167', 'ACTIVE', '2019-09-15 18:53:41.167', 'admin@admin.com', 'admin', 'admin', '$2a$10$x8mBwhuTCdvd6BC5BM90u.Qkmy26j5cWSXND2DjmVVsz2gqqV2hSm',
            'admin');

            

insert into user_roles(
            user_id, role_id)
    VALUES (9999, 1);

