import app.dto.StatisticItem;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class StatisticItemTest {
    @Test
    public void stItem() {
        StatisticItem statisticItem = new StatisticItem();
        statisticItem.setPrice(12F);
        statisticItem.setCategory("Car");

        StatisticItem statisticItem1 = new StatisticItem();
        statisticItem1.setPrice(22F);
        statisticItem1.setCategory("Car");


        Set<StatisticItem> setItems = new HashSet<>();

        setItems.add(statisticItem);

        setItems.contains(statisticItem1);

        statisticItem.equals(statisticItem1);

        Assert.assertEquals(statisticItem, statisticItem1);

    }
}
