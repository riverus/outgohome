package app.controller.v1;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@AutoConfigureMockMvc(secure = false)
@EnableJpaRepositories(basePackages = "app.dao")
@RunWith(SpringJUnit4ClassRunner.class)
public class RecordControllerV1Test {

    @InjectMocks
    RecordControllerV1 recordControllerV1;


    MockMvc mockMvc;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.standaloneSetup(recordControllerV1).build();
    }

    @Test
    public void getAllRecords() {
//        Mockito.when(recordRepository.findAllByDateBetweenAndUserId(Mockito.any(), Mockito.any(), Mockito.anyLong()))
//                .thenReturn(Arrays.asList(new Record()));
        try {
            mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/records").param("dateFrom", "01-10-1000"));

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}