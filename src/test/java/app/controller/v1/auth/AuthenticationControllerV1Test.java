package app.controller.v1.auth;

import app.dto.RegisterRequestDto;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

public class AuthenticationControllerV1Test {
    private static AuthenticationControllerV1 authenticationControllerV1;
    @BeforeClass
    public static void init() {
        authenticationControllerV1 = new AuthenticationControllerV1(null, null, null);
    }

    @Test
    public void testLessThanEightCharactersInPassword() {
        RegisterRequestDto registerRequestDto = new RegisterRequestDto();
        registerRequestDto.setEmail("valid@email.ru");
        registerRequestDto.setPassword("123qwe");
        ResponseEntity responseEntity = authenticationControllerV1.register(registerRequestDto);

        Assert.assertEquals(responseEntity.getStatusCode().value(), 400);
    }
    @Test
    public void testAllNumbersInPassword() {
        RegisterRequestDto registerRequestDto = new RegisterRequestDto();
        registerRequestDto.setEmail("valid@email.ru");
        registerRequestDto.setPassword("12345678");
        ResponseEntity responseEntity = authenticationControllerV1.register(registerRequestDto);

        Assert.assertEquals(responseEntity.getStatusCode().value(), 400);
    }

    @Test
    public void testAllCharactersInPassword() {
        RegisterRequestDto registerRequestDto = new RegisterRequestDto();
        registerRequestDto.setEmail("valid@email.ru");
        registerRequestDto.setPassword("qweEqweqQQWQeqweqQE");
        ResponseEntity responseEntity = authenticationControllerV1.register(registerRequestDto);

        Assert.assertEquals(responseEntity.getStatusCode().value(), 400);
    }

    @Test
    public void testEmailNotValid() {
        RegisterRequestDto registerRequestDto = new RegisterRequestDto();
        registerRequestDto.setEmail("notvalid@email");
        ResponseEntity responseEntity = authenticationControllerV1.register(registerRequestDto);

        Assert.assertEquals(responseEntity.getStatusCode().value(), 400);
    }
}